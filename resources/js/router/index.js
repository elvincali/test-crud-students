import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router);

const router = new Router({
    mode: 'history',
    scrollBehavior: () => ({ y: 0 }),
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import ('../views/Index.vue'),
        },
    ],
});

export default router;
