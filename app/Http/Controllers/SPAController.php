<?php

namespace App\Http\Controllers;

final class SPAController extends Controller
{
    public function __invoke(){
        return view('layouts.master');
    }
}
