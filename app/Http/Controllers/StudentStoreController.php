<?php

namespace App\Http\Controllers;

use App\DTOs\StudentStoreData;
use App\Http\Requests\StudentStoreRequest;
use App\Services\StudentStoreService;
use App\Support\BaseResponse;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

final class StudentStoreController extends Controller
{
    private StudentStoreService $service;

    public function __construct(StudentStoreService $service)
    {
        $this->service = $service;
    }

    public function __invoke(StudentStoreRequest $request): JsonResponse
    {
        $response = new BaseResponse();

        try {
            DB::beginTransaction();
            $data = new StudentStoreData(
                fullName: $request->get('fullName'),
                age: $request->get('age'),
                dateBirth: Carbon::parse($request->get('dateBirth')),
                enrollmentDate: Carbon::parse($request->get('enrollmentDate')),
                cost: $request->get('cost'),
            );

            $this->service->execute($data);

            $response->message = 'Creado Correctamente';

            DB::commit();
            return new JsonResponse($response);
        } catch (\Throwable $e) {
            DB::rollBack();
            $response->errorResponse(($e->getCode() == 0) ? 2 : $e->getCode(), ($e->getCode() == 0) ? "Error desconocido." : $e->getMessage());
            return new JsonResponse($response, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
