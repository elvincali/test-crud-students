<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Resources\StudentIndexCollection;
use App\Support\BaseResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

final class StudentIndexController extends Controller
{
    public function __invoke(Request $request): JsonResponse
    {
        $response = new BaseResponse();

        $students = new StudentIndexCollection(Student::all());

        $response->data = $students;

        return new JsonResponse($response);
    }
}
