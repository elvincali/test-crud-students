<?php

namespace App\Http\Requests;

use App\Rules\ProportionalWithEnrollmentDate;
use App\Rules\VerifyWithDateBirth;
use Illuminate\Foundation\Http\FormRequest;

final class StudentStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'fullName' => [
                'required',
            ],
            'age' => [
                'required',
                'numeric',
                'min:18',
                new VerifyWithDateBirth($this->get('dateBirth')),
            ],
            'dateBirth' => [
                'required',
                'date',
            ],
            'enrollmentDate' => [
                'required',
                'date',
                'after:dateBirth'
            ],
            'cost' => [
                'required',
                'numeric',
                'min:0',
                new ProportionalWithEnrollmentDate($this->get('enrollmentDate')),
            ],
        ];
    }

    public function messages(): array
    {
        return [
            //
        ];
    }
}
