<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

final class Student extends Model
{
    use SoftDeletes;

    protected $table = 'students';

    protected $casts = [
        'date_birth' => 'date',
        'enrollment_date' => 'date',
    ];
}
