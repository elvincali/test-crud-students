<?php

namespace App\Services;

use App\DTOs\StudentStoreData;
use App\Models\Student;

final class StudentStoreService
{
    public function execute(StudentStoreData $data): void
    {
        $student = new Student();
        $student->full_name = $data->fullName;
        $student->age = $data->age;
        $student->date_birth = $data->dateBirth;
        $student->enrollment_date = $data->enrollmentDate;
        $student->cost = $data->cost;
        $student->save();
    }
}
