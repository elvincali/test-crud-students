<?php

namespace App\DTOs;

use Carbon\Carbon;

final class StudentStoreData
{
    public function __construct(
        public string $fullName,
        public int $age,
        public Carbon $dateBirth,
        public Carbon $enrollmentDate,
        public float $cost
    ){}

}
