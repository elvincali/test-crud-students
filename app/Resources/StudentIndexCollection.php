<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

final class StudentIndexCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->transform(function($row) {
            return [
                'id' => $row->id,
                'fullName' => $row->full_name,
                'age' => $row->age,
                'dateBirth' => $row->date_birth->format('d/m/Y'),
                'enrollmentDate' => $row->enrollment_date->format('d/m/Y'),
                'cost' => $row->date_birth,
            ];
        });
    }
}
