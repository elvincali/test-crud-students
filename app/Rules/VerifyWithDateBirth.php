<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Log;

class VerifyWithDateBirth implements Rule
{
    private string $dateBirth;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $dateBirth)
    {
        $this->dateBirth = $dateBirth;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $dateBirth = Carbon::parse($this->dateBirth);

        return $dateBirth->age == $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'La edad no coincide con la fecha de nacimiento.';
    }
}
