<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class ProportionalWithEnrollmentDate implements Rule
{
    private string $enrollmentDate;

    private const AMOUNT_YEAR = 100;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $enrollmentDate)
    {
        $this->enrollmentDate = $enrollmentDate;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $years = Carbon::parse($this->enrollmentDate)->age;

        return ($years * self::AMOUNT_YEAR) == $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'El costo no es proporcional a la fecha de inscripcion';
    }
}
