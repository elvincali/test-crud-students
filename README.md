## Instalacion

1.- Instalar las dependencias laravel

```bash
composer install
```

2.- Generar un archivo .env a partir del archivo .env.example

3.- Generar un Application Key

```bash
php artisan key:generate
```
4.- Instalar las dependencias npm (no ejecutar upgrade)
```bash
npm install
```

## Uso

1.- Iniciar el servidor

```bash
php artisan serve
```

2.- Iniciar la compilacion, observando cambios (en una segunda terminal)

```bash
npm run watch
```

En caso que el comando anterior no observara cambios intente usar la alterntiva
```bash
npm run watch-poll
``` 


## Script DB
```bash
create table students(
id int unsigned not null primary key auto_increment,
full_name varchar(190) not null,
age tinyint unsigned not null,
date_birth date not null,
enrollment_date date not null,
cost decimal not null,
created_at timestamp null,
updated_at timestamp null,
deleted_at timestamp null
);
```
este es un cambio para generar conflicto